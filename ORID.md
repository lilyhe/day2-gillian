# day2-Gillian

## Objective: 
We can't just get the requirements and start coding. The process of software development should be Requirement->Clarify->Tasking->Context Map->Coding. Code can be written in a more organized and efficient way once the requirements are clearly clarified and the tasks are broken down.
Tasking and Context Map were emphasized.
Tasking means dividing a task into multiple independent subtasks and completing each subtask to promote the completion of the whole task. Therefore, the principle of Tasking is that complex tasks are simplified, each task is independent, executable and verifiable.
Context Map is a very useful tool during Tasking. Context Map  visualizes the process of Tasking and makes it more hierarchical and clear. Because we need to name each subtask as well as the input and output in Content Map, we also learned the naming rules about development.
I also briefly reviewed the use of Git. In daily study, develop the habit of committing in small steps, and test changes before committing.

## Reflective: 
Fulfilled but a little difficult

## Interpretive: 
That I felt difficult is mainly because I am not familiar enough with java. Therefore I always doubt the feasibility of subtasks. However, when I did exercises to the best of my ability, I found that the programming became clearer and more organized. And the code is more readable than ever.

## Decisional: 
Record the java difficulties encountered in class at any time, and study after class. Review the exercises more often, understand the idea of Tasking and internalize it.
